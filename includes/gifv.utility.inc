<?php

/**
 * @file
 * Holds common utility functions used across the module and submodules.
 */

/**
 * A helper function which will create video file(s) using the default wrapper.
 *
 * @param array &$file
 *   The target file array.
 * @param array $settings
 *   An optional, explicit array of settings to use. Defaults to the saved
 *   configuration options set on the module configuration page.
 * @param array $use_video_formats
 *   An optional, explicit array of video formats to use. Overrides options set
 *   in $settings.
 *
 * @return mixed
 *   If an error occurred, the error will be logged and this function will
 *   return FALSE. Otherwise, it will return an adjusted $file parameter.
 */
function _gifv_create_video(array &$file, array $settings = array(), array $use_video_formats = array()) {
  // Verify that $file is correct.
  _gifv_get_image_info($file);

  // If the image isn't a gif image, quit.
  if ($file['mime_type'] !== 'image/gif' && $file['source_image']['mime_type'] !== 'image/gif') {
    return FALSE;
  }

  // If this file is local and not animated, quit.
  if ($file['location'] == 'local' && (!isset($file['source_image']) && isset($file['is_animated']) && !$file['is_animated'] || isset($file['source_image']) && !$file['source_image']['is_animated'])) {
    return FALSE;
  }

  // Get the default settings and merge them with the configured settings.
  $defaults = _gifv_get_default_settings();
  $settings = array_merge($defaults, $settings);

  // Get the default wrapper.
  $wrapper_name = isset($settings['wrappers']['gifv_wrapper']) ? $settings['wrappers']['gifv_wrapper'] : _gifv_get_default_wrapper_name();
  $default_wrapper = $settings['wrappers'][$wrapper_name];
  $default_wrapper = array_merge(_gifv_get_default_wrapper(), $default_wrapper);
  $default_wrapper_id = $default_wrapper['id'];

  // Get the default video formats and merge them with the configured video
  // formats.
  $default_video_formats = $default_wrapper['video_formats'];
  if (!empty($settings) && !empty($settings['wrappers'][$default_wrapper_id]['video_formats'])) {
    $video_formats = array_keys($settings['wrappers'][$default_wrapper_id]['video_formats']);
    $video_formats = array_flip($video_formats);
  }
  if (empty($video_formats)) {
    $video_formats = $default_video_formats;
  }
  else {
    $use_video_formats = array_flip($use_video_formats);
    // Intersect the default video formats with the video formats provided in
    // $settings parameter.
    $video_formats = array_intersect_key($default_video_formats, $video_formats);
    // If convert to all video paths in configuration isn't set and there's a
    // provided list of video formats to use, intersect the two arrays.
    if (!$settings['gifv_convert_on_path_all'] && count($use_video_formats)) {
      $video_formats = array_intersect_key($video_formats, $use_video_formats);
    }
  }

  // Determine if video derivative support is enabled and, if it is, configure
  // some things needed for the video derivative processing.
  $arguments = array();
  // Condition: file is local and gifv derivative support is enabled and the
  // image module is enabled.
  if ($file['location'] == 'local' && $settings['derivative_support']['gifv_support_image_derivatives'] && module_exists('image')) {
    // If this is a derivative style from the image module.
    if ($file['is_derivative']) {
      // If the method of supporting derivative images is to just convert the
      // derivative image.
      $type = $settings['derivative_support']['gifv_support_image_derivatives_type'];
      $image_style = image_style_load($file['image_style']);
      if ($type == 'convert') {
        // If that image style derivative doesn't exist, invoke its creation.
        // Then, we'll just use it as an input file.
        if (empty($file['fid']) && !file_exists($file['uri'])) {
          image_style_create_derivative(image_style_load($file['image_style']), $file['source_paths']['uri'], image_style_path($file['image_style'], $file['source_paths']['uri']));
        }
      }
      elseif ($type == 'copy_image_effects') {
        if (!function_exists('image_style_load')) {
          module_load_include('module', 'image');
        }

        // @todo support rotating and desaturating filters.
        $image_info = image_get_info($file['absolute_path']);
        foreach ($image_style['effects'] as $effect) {
          $effect_name = str_replace('image_', '', $effect['name']);
          switch ($effect_name) {
            case 'scale':
            case 'scale_and_crop':
            case 'resize':
              $arguments[$effect_name] = array(
                'width' => isset($effect['data']['width']) ? $effect['data']['width'] : NULL,
                'height' => isset($effect['data']['height']) ? $effect['data']['height'] : NULL,
              );
              break;

            case 'crop':
              $arguments[$effect_name] = array(
                'width' => isset($effect['data']['width']) ? $effect['data']['width'] : NULL,
                'height' => isset($effect['data']['height']) ? $effect['data']['height'] : NULL,
              );
              $anchor = isset($effect['data']['anchor']) ? $effect['data']['anchor'] : 'center-center';
              $parts = explode('-', $anchor);
              $horizontal = $parts[0];
              $vertical = $parts[1];
              $newanchor = array();
              switch ($horizontal) {
                case 'left':
                  $newanchor['left'] = 0;
                  break;

                case 'center':
                  $newanchor['left'] = round($image_info['width'] / 2);
                  break;

                case 'right':
                  $newanchor['left'] = $image_info['width'];
                  break;

              }
              switch ($vertical) {
                case 'top':
                  $newanchor['top'] = 0;
                  break;

                case 'center':
                  $newanchor['top'] = round($image_info['height'] / 2);
                  break;

                case 'bottom':
                  $newanchor['top'] = $image_info['height'];
                  break;

              }
              $arguments[$effect_name] += array('anchor' => $newanchor);
              break;

          }
        }
      }
    }
  }

  _gifv_generate_derivative_paths($file, $video_formats);

  // If no video paths were set, quit.
  if (!count($file['video_paths'])) {
    return FALSE;
  }

  try {
    $save_paths = array();
    // Only work with paths that don't exist.
    foreach ($file['video_paths'] as $type => $path) {
      if (!file_exists($path['uri'])) {
        $save_paths[$type] = $file['video_paths'][$type];
        // Make sure the parent folder exists.
        $directory = str_replace(basename($save_paths[$type]['absolute_path']), '', $save_paths[$type]['absolute_path']);
        if (!file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
          drupal_set_message(t('Failed to create the directory at :directory', array(':directory' => $directory)), FALSE);
          // @todo error reporting.
        }
      }
    }
    // If there are paths that don't exist, continue generation.
    if (count($save_paths)) {
      // Ensure the source image exists.
      if ($file['location'] == 'remote') {
        // Even if remote image saving is disabled, we still need to save a copy
        // of the image, if only temporarily, while we create the videos from
        // the source image. If remote image saving is disabled, the image will
        // be deleted in the cleanup function.
        if (!_gifv_save_remote_image($file, $settings['remote']['gifv_save_remote'])) {
          drupal_set_message(t('Failed to download the image at :path.', array(':path' => $file['absolute_path'])), FALSE);
          return FALSE;
        }
      }

      $form_settings = $settings['wrappers'][$default_wrapper_id]['video_formats'];
      $form_settings = _gifv_flatten_options_array($form_settings);

      // Allow any other modules to alter the $file before its counterpart
      // videos are generated.
      if ($default_wrapper['implementation']->save(isset($file['source_image']) ? $file['source_image'] : $file, $save_paths, $arguments, $form_settings)) {
        _gifv_file_cleanup($file, $settings['remote']['gifv_save_remote']);
        return $file;
      }
    }
  }
  catch (Exception $e) {
    drupal_set_message(t('Failed to create video file(s) from gif image at :path. Error: :error', array(':path' => $file['absolute_path'], ':error' => $e)), 'error');
    return FALSE;
    // @todo print exception to watchdog.
  }
}

/**
 * Utility function to flatten a form element array.
 *
 * This value will be returned to the wrapper. Removes any properties and sets
 * non-parent array elements to their default values.
 *
 * @param array $array
 *   The form element array.
 *
 * @return array
 *   The flattened form element array.
 */
function _gifv_flatten_options_array(array $array) {
  if (!is_array($array)) {
    throw new Error('The parameter provided was not an array.');
  }

  $return = array();
  $keys = element_children($array);
  foreach ($keys as $key) {
    if (is_array($array[$key])) {
      if (!empty($array[$key]['#default_value'])) {
        $return[$key] = $array[$key]['#default_value'];
      }
      else {
        $return[$key] = _gifv_flatten_options_array($array[$key]);
      }
    }
    else {
      $return[$key] = $array[$key];
    }
  }
  return $return;
}

/**
 * A helper function for getting a wrapper definitions.
 *
 * This function will, by default, return all wrapper definitions from all
 * enabled modules. By passing an array of wrapper ids, you can optionally
 * specify which wrappers to load.
 *
 * @param array $use_wrappers
 *   An optional array of explicit wrappers to retrieve.
 *
 * @return array
 *   An array of wrapper's, formatted as [module_name] => ([wrapper_id] =>
 *   [wrapper]).
 */
function _gifv_get_wrappers(array $use_wrappers = array()) {
  $saved_wrapper_options = variable_get('gifv_wrapper_options', array());
  $saved_video_formats = variable_get('gifv_video_formats', array());

  $wrappers = &drupal_static(__FUNCTION__);
  $continue = FALSE;
  $cache = FALSE;
  if (!isset($wrappers)) {
    $continue = TRUE;
  }
  else {
    foreach ($use_wrappers as $wrapper) {
      $parts = explode('__', $wrapper);
      $module = $parts[0];
      $wrapper_id = $parts[1];
      if (!isset($wrappers[$module]) || (isset($wrappers[$module]) && !isset($wrappers[$module][$wrapper_id]))) {
        // We don't have information for at least one of the wrappers requested.
        $continue = TRUE;
        break;
      }
    }
  }
  if ($continue) {
    $continue = FALSE;
    if ($cache = cache_get('gifv_wrappers_info')) {
      if (count($cache->data) === 0) {
        $continue = TRUE;
      }
      else {
        foreach ($use_wrappers as $wrapper) {
          $parts = explode('__', $wrapper);
          $module = $parts[0];
          $wrapper_id = $parts[1];
          if (!isset($cache->data[$module]) || (isset($cache->data[$module]) && !isset($cache->data[$module][$wrapper_id]))) {
            // We don't have information for at least one of the wrappers
            // requested.
            $continue = TRUE;
            break;
          }
        }
      }
      if (!$continue) {
        $wrappers = $cache->data;
      }
    }
    elseif (!$cache || !$continue) {
      $wrappers = array();
      if (count($use_wrappers) === 0) {
        $wrappers = module_invoke_all('gifv_wrapper');
      }
      else {
        foreach ($use_wrappers as $w) {
          $split = explode('__', $w);
          $module = $split[0];
          $formatter = $split[1];
          $wrapper_result = module_invoke($module, 'gifv_wrapper');
          $wrappers[$module] = array_intersect_key($wrapper_result[$module], array($formatter => ''));
        }
      }

      foreach ($wrappers as $module => $module_wrapper) {
        foreach ($module_wrapper as $wrapper_id => $wrapper) {
          $wrapper['id'] = $module . '__' . $wrapper_id;
          $name = !empty($wrapper['name']) ? '\'' . $wrapper['name'] . '\'' : '';
          if (empty($wrapper['file'])) {
            drupal_set_message(t('The wrapper definition :name from module :module did not specify a file to use.', array(':name' => $name, ':module' => $module)), 'error', FALSE);
            return FALSE;
          }

          // Load the wrapper to get some information about it.
          module_load_include('inc', $module, $wrapper['file']);
          if (empty($wrapper['class_name'])) {
            $wrapper['class_name'] = basename($wrapper['file']);
          }
          if (!class_exists($wrapper['class_name'])) {
            drupal_set_message(t('The wrapper definition :name from module :module either provided an incorrect class name or did not provide one. Tried loading class :class_name.', array(
              ':name' => $name,
              ':module' => $module,
              ':class_name' => $wrapper['class_name'],
            )), 'error', FALSE);
            return FALSE;
          }
          $wrapper['implementation'] = new $wrapper['class_name'](!empty($saved_wrapper_options[$module]) && !empty($saved_wrapper_options[$module][$wrapper_id]) ? $saved_wrapper_options[$module][$wrapper_id] : array(), !empty($saved_video_formats[$wrapper['id']]) ? $saved_video_formats[$wrapper['id']]['video_formats'] : array());

          if (empty($wrapper['name'])) {
            $wrapper['name'] = $wrapper['implementation']->getName();
          }

          if (empty($wrapper['description'])) {
            $wrapper['description'] = $wrapper['implementation']->getDescription();
          }

          if (empty($wrapper['video_formats'])) {
            $wrapper['video_formats'] = $wrapper['implementation']->getAvailableVideoFormatOptions();
          }

          if (!empty($saved_wrapper_options[$module]) && !empty($saved_wrapper_options[$module][$wrapper_id])) {
            $wrapper['options'] = array_merge($saved_wrapper_options[$module][$wrapper_id]);
          }
          if (method_exists($wrapper['implementation'], 'getOptions')) {
            $wrapper_options = $wrapper['implementation']->getOptions();
            if (count($wrapper_options)) {
              $children = element_children($wrapper['options']);
              foreach ($children as $child) {
                if (!is_array($wrapper['options'][$child]) && !empty($wrapper_options[$child])) {
                  $wrapper_options[$child]['#default_value'] = $wrapper['options'][$child];
                }
              }
              $wrapper['options'] = count($wrapper['options']) > 0 ? array_merge($wrapper['options'], $wrapper_options) : $wrapper_options;
            }
          }
          uasort($wrapper['video_formats'], '_gifv_video_format_weight_arraysort');

          if (method_exists($wrapper['implementation'], 'validateForm') && is_callable($validate_function = $wrapper['class_name'] . '::validateForm')) {
            $wrapper['validate_hook'] = $validate_function;
          }

          $wrappers[$module][$wrapper_id] = $wrapper;
        }
      }
      cache_set('gifv_wrappers_info', $wrappers, 'cache');
    }
  }

  // Make sure the default wrapper's class is instantiated. This needs to be
  // done when the wrappers were loaded from cache.
  if ($cache || $continue) {
    $default_wrapper = _gifv_get_default_wrapper_name();
    $parts = explode('__', $default_wrapper);
    $module = $parts[0];
    $wrapper_id = $parts[1];
    $default_wrapper = &$wrappers[$module][$wrapper_id];
    if (gettype($default_wrapper['implementation']) == 'object' && !is_object($default_wrapper['implementation'])) {
      // This usually occurs when the implementation object isn't complete
      // because it was loaded from cache.
      // Its type is __PHP_Incomplete_Class Object.
      $parts = explode('__', $default_wrapper['id']);
      $module = $parts[0];
      $wrapper_name = $parts[1];
      module_load_include('inc', $module, $default_wrapper['file']);
      $saved_wrapper_settings = variable_get('gifv_wrapper_options', array());
      if (isset($saved_wrapper_settings[$module]) && isset($saved_wrapper_settings[$module][$wrapper_name])) {
        $saved_wrapper_settings = $saved_wrapper_settings[$module][$wrapper_name];
      }
      if (isset($saved_video_formats[$default_wrapper['id']])) {
        $saved_video_formats = $saved_video_formats[$default_wrapper['id']]['video_formats'];
      }
      $default_wrapper['implementation'] = new $default_wrapper['class_name']($saved_wrapper_settings, $saved_video_formats);
    }
  }

  return $wrappers;
}

/**
 * A helper function for retrieving the default wrapper's name.
 *
 * @return string
 *   The default wrapper's name, formatted as [module]__[wrapper_id]
 */
function _gifv_get_default_wrapper_name() {
  return variable_get('gifv_wrapper', module_exists('gifv_default_wrapper') ? 'gifv_default_wrapper__ffmpeg' : '');
}

/**
 * A helper function for retrieving the default wrapper.
 *
 * @return array
 *   The default wrapper.
 */
function _gifv_get_default_wrapper() {
  $default_wrapper = _gifv_get_default_wrapper_name();
  if (!empty($default_wrapper)) {
    $default_wrapper = _gifv_get_wrappers(array($default_wrapper));
    $shift = array_shift($default_wrapper);
    $wrapper = array_shift($shift);
    return $wrapper;
  }
  return NULL;
}

/**
 * Helper function for returning the class instance of the default wrapper.
 *
 * @return object
 *   An instantation of the wrapper's class.
 */
function _gifv_get_default_wrapper_instance() {
  $wrapper = _gifv_get_default_wrapper();
  return $wrapper['implementation'];
}

/**
 * Helper function for retrieving the default wrapper's video formats.
 *
 * @return array
 *   An array of the default wrapper's video formats.
 */
function _gifv_get_default_wrapper_video_formats() {
  $wrapper = _gifv_get_default_wrapper();
  return array_filter($wrapper['video_formats'], '_gifv_video_format_filter');
}

/**
 * Creates and returns an array with needed elements for this module.
 *
 * @param mixed &$file
 *   Either a string representing the path location of the file, or a file
 *   object.
 *
 * @return array
 *   An array of needed elements for this module.
 */
function _gifv_get_image_info(&$file) {
  // $file could either be a string, object, or array depending on where we call
  // the function from. If it's an object or array we assume the uri
  // property/element is available.
  if (is_object($file)) {
    $file = (array) $file;
  }
  if (is_array($file)) {
    if (isset($file['processed']) && $file['processed']) {
      return $file;
    }
    if (isset($file['uri'])) {
      $src = $file['uri'];
    }
  }
  elseif (is_string($file)) {
    $src = $file;
    $file = array();
  }
  $match = preg_match('/^([public|private|temporary]+\:\/\/)([\w\/\.\-\+]+)/i', $src, $matches);
  $public_dir = variable_get('file_public_path', conf_path() . '/files');
  $private_dir = variable_get('file_private_path', conf_path() . '/files/private');
  if ($match !== FALSE && count($matches) == 3) {
    $file['location'] = 'local';
    $file['uri'] = $src;
    if ($wrapper = file_stream_wrapper_get_instance_by_uri($matches[1])) {
      $file['absolute_path'] = $wrapper->realpath() . '/' . $matches[2];
    }
  }
  else {
    // Determine the location of this file, either local or remote.
    if ((strpos($src, base_path()) === 0 || strpos($src, $public_dir) === 0) && strpos($src, file_directory_temp()) === FALSE) {
      $file['location'] = 'local';
      $file['uri'] = 'public://' . preg_replace('/^\/?(' . preg_quote($public_dir, '/') . ')?\/?/i', '', $src);
      if ($wrapper = file_stream_wrapper_get_instance_by_uri('public://')) {
        $file['absolute_path'] = $wrapper->realpath() . ($src[0] !== '/' ? '/' : '') . $src;
      }
    }
    // If this file is "remote" but actually hosted on the same server.
    elseif (preg_match('/https?\:\/\/' . preg_quote($_SERVER['HTTP_HOST'] . base_path(), '/') . '/', $src)) {
      $file['location'] = 'local';
      $parse = parse_url($src);
      $file['uri'] = 'public://' . preg_replace('/^\/?' . preg_quote($public_dir, '/') . '\//i', '', $parse['path']);
      $file['absolute_path'] = $_SERVER['DOCUMENT_ROOT'] . $parse['path'];
    }
    // Otherwise the file is truly remote.
    elseif (strpos($src, 'http') === 0) {
      // CDN support.
      // Remote, except when it's on a cdn server.
      if (module_exists('cdn')) {
        // If the cdn module is enabled, see if this image is on the cdn.
        $cdnmode = variable_get('cdn_mode');
        $get_servers_function = 'cdn_' . $cdnmode . '_get_servers';
        module_load_include('inc', 'cdn', 'cdn.basic');
        $servers = $get_servers_function($src);
        foreach ($servers as $server) {
          if (strpos($src, $server['server']) !== FALSE) {
            // Use the location on our server instead of the location on the cdn
            // server.
            $localsrc = str_replace($server['server'], '', $src);
            if (file_exists($localsrc)) {
              $file['source'] = $localsrc;
              $file['location'] = 'local';
            }
            break;
          }
        }
      }
      else {
        $file['location'] = 'remote';
        // Set uri and absolutepath to temporary directory where image will
        // eventually be saved to.
        $file['uri'] = 'temporary://' . basename($src);
        $file['absolute_path'] = file_directory_temp() . '/' . basename($src);
        $file['source_image'] = $src;
        $headers = @get_headers($src, 1);
        if ($headers !== FALSE) {
          $file['mime_type'] = $headers['Content-Type'];
        }
      }
    }
    // Everything has failed, I don't know what to do.
    else {
      $file['location'] = 'unknown';
    }
  }

  // If this file is local.
  if ($file['location'] == 'local') {
    // Determine if this file is an image derivative.
    $match = preg_match('/styles\/([\w\-]+)\/(public|private)\//i', $file['uri'], $matches);
    if (!isset($file['is_derivative'])) {
      $file['is_derivative'] = $match;
    }
    if ($file['is_derivative']) {
      $file['image_style'] = $matches[1];
      $file['scheme'] = isset($matches[2]) ? $matches[2] : 'public';
      // "source_paths" holds path information about the root file, as opposed
      // to the derivative file.
      $basename = str_replace($file['scheme'] . '://' . 'styles/' . $file['image_style'] . '/' . $file['scheme'] . '/', '', $file['uri']);
      $file['source_paths'] = array(
        'uri' => $file['scheme'] . '://' . $basename,
        'absolute_path' => $_SERVER['DOCUMENT_ROOT'] . '/' . ($file['scheme'] == 'public' ? $public_dir : $private_dir) . '/' . $basename,
      );
      $file['source_paths']['exists'] = file_exists($file['source_paths']['uri']);
    }
    else {
      $file['source_paths'] = array(
        'uri' => $file['uri'],
        'absolute_path' => $file['absolute_path'],
      );
      $file['source_paths']['exists'] = file_exists($file['source_paths']['uri']);
    }

    // Depending on where $file was originally defined, because of differences
    // in Drupal 7's file object handling versus its `image_get_info` function,
    // the mime property might be in "filemime" instead of "mime_type".
    if (isset($file['filemime'])) {
      $file['mime_type'] = $file['filemime'];
      unset($file['filemime']);
    }

    // Depending on where $file was originally defined, because of differences
    // in Drupal 7's file object handling versus its `image_get_info` function,
    // the size property might be in "filesize" instead of "file_size".
    if (isset($file['filesize'])) {
      $file['file_size'] = $file['filesize'];
      unset($file['filesize']);
    }

    // Fill out the rest of the information using Drupal's `image_get_info`
    // function.
    $info = image_get_info($file['source_paths']['uri']);
    if ($info !== FALSE) {
      $file += $info;
    }
  }

  // Specify if the local image exists.
  if ($file['location'] == 'local' && !isset($file['exists'])) {
    $file['exists'] = file_exists($file['uri']);
  }

  // Specify the image's file extension.
  if (!isset($file['extension'])) {
    $file['extension'] = pathinfo($file['absolute_path'], PATHINFO_EXTENSION);
  }

  // If this file is a video file, we'll want information on the source image.
  if (!isset($file['source_image']) && $file['extension'] != 'gif') {
    $video_formats = _gifv_get_default_wrapper_video_formats();
    $types = array_keys($video_formats);
    if (in_array($file['extension'], $types)) {
      $file['source_image'] = array(
        'uri' => preg_replace('/\.' . $file['extension'] . '$/', '.gif', $file['source_paths']['uri']),
        'absolute_path' => preg_replace('/\.' . $file['extension'] . '$/', '.gif', $file['source_paths']['absolute_path']),
        'is_derivative' => $file['is_derivative'],
        'style' => $file['image_style'],
      );
      $file['source_image']['exists'] = file_exists($file['source_image']['uri']);
      $file['source_image']['is_animated'] = _gifv_is_gif_animated($file['source_image']['uri']);

      $info = image_get_info($file['source_image']['uri']);
      // If the file doesn't exist, it will return false.
      if ($info) {
        $file['source_image']['exists'] = TRUE;
        $file['source_image'] += $info;
      }
      else {
        $file['source_image']['exists'] = FALSE;
      }
    }
  }

  // If the mime_type still isn't set, we'll just make a guess based on the file
  // extension.
  if (!isset($file['mime_type'])) {
    if ($file['extension'] == 'gif') {
      $file['mime_type'] = 'image/gif';
    }
    else {
      // @todo should probably use the video formatters' information to
      // determine this.
      $file['mime_type'] = 'video/' . $file['extension'];
    }
  }

  // If this is a local gif image, determine if it's animated.
  if ($file['location'] == 'local' && $file['extension'] == 'gif' && !isset($file['is_animated'])) {
    _gifv_is_gif_animated($file);
  }

  // Precautionary flag so the work isn't redone.
  $file['processed'] = TRUE;

  return $file;
}

/**
 * Helper function to generate video derivative paths.
 *
 * This function will generate the video derivative paths for the provided video
 * formats for a gif image. Very simple string replacement function, no file
 * path verification/validation.
 *
 * @param string &$file
 *   The source gif image path.
 * @param array $video_formats
 *   The enabled video formats.
 *
 * @return array
 *   The derivative video paths.
 */
function _gifv_generate_derivative_paths(&$file, array $video_formats) {
  $location = variable_get('gifv_videos_filepath_enable', FALSE) ? variable_get('gifv_videos_filepath', 'public://') : 'public://';
  $public_file_dir = variable_get('file_public_path', conf_path() . '/files');
  if (empty($file['video_paths'])) {
    $file['video_paths'] = array();
  }
  if (is_array($video_formats) && count($video_formats) > 0) {
    foreach ($video_formats as $extension => $enabled) {
      // TODO clean this up
      // Someone else might have filled this stuff in already, so only do this
      // if it isn't set.
      if (empty($file['video_paths'][$extension])) {
        $file['video_paths'][$extension] = array();
      }
      if (empty($file['video_paths'][$extension]['uri'])) {
        $file['video_paths'][$extension]['uri'] = $location[0] == '/' ? $location : !empty($file['uri']) ? str_replace('temporary://', $location, str_replace('.gif', '.' . $extension, $file['uri'])) : $location . str_replace('.gif', '.' . $extension, basename($file['source_paths']['uri']));
      }
      if (empty($file['video_paths'][$extension]['absolute_path'])) {
        $file['video_paths'][$extension]['absolute_path'] = $location[0] == '/' ? $location : !empty($file['absolute_path']) ? str_replace(file_directory_temp(), $public_file_dir, str_replace('.gif', '.' . $extension, $file['absolute_path'])) : $_SERVER['DOCUMENT_ROOT'] . '/' . str_replace('public://', $public_file_dir, $file['source_paths']['uri']) . '/' . str_replace('.gif', '.' . $extension, basename($file['source_paths']['uri']));
      }
    }
  }
  return $file;
}

/**
 * Helper function to save remote images to the temporary directory.
 *
 * @param string &$file
 *   File object of the source image.
 * @param bool $save
 *   TRUE to save the image to the server.
 *
 * @return mixed
 *   Returns the adjusted $file object on success, or FALSE otherwise.
 */
function _gifv_save_remote_image(&$file, $save = TRUE) {
  $name = basename($file['source_image']);
  $file['destination'] = ($save ? 'public://' : file_directory_temp() . '/') . $name;
  if (file_exists($file['destination'])) {
    return $file;
  }
  if (system_retrieve_file($file['source_image'], $file['destination'], $save) !== FALSE) {
    $file['uri'] = $save ? $file['destination'] : 'temporary://' . $name;
    $file['absolute_path'] = $file['destination'];
    $file = array_merge($file, image_get_info($file['destination']));
    return $file;
  }
  else {
    drupal_set_message(t('Failed to download image at :source_image.', array(':source_image' => $file['source_image'])), 'status', FALSE);
    return FALSE;
  }
}

/**
 * Helper function to clean up a file object.
 *
 * Perform any needed cleaning to files. Generally used to remove remote image
 * files if the 'save remote images' configuration option is disabled.
 *
 * @param object $file
 *   The file object to clean.
 * @param bool $save
 *   TRUE if saving file.
 * @param bool $managed
 *   TRUE if this file is managed by Drupal.
 *
 * @return bool
 *   TRUE if the file cleanup was successful.
 */
function _gifv_file_cleanup($file, $save = TRUE, $managed = FALSE) {
  if (!$save) {
    if (!$managed) {
      return file_unmanaged_delete($file['destination']);
    }
    else {
      return file_managed_delete($file);
    }
  }
  return TRUE;
}

/**
 * Helper function for replacing <img> tags with <video> tags.
 *
 * @param string $text
 *   The original text.
 *
 * @return string
 *   The new text.
 */
function _gifv_replace_img_with_video($text, $src, $file, $settings) {
  $output = theme('gifv_image_formatter', array('item' => array_merge($file, array('gifv' => array('settings' => $settings)))));
  $text = str_replace($src, $output, $text);
  return $text;
}

/**
 * Helper function for sorting video format weights.
 */
function _gifv_video_format_weight_arraysort($a, $b) {
  if (isset($a['weight']) && isset($b['weight'])) {
    return $a['weight'] < $b['weight'] ? -1 : 1;
  }
  return 0;
}

/**
 * Helper function for filtering out disabled video formats.
 *
 * @param array $element
 *   The video format to check.
 *
 * @return bool
 *   TRUE if this video format is enabled, FALSE otherwise.
 */
function _gifv_video_format_filter(array $element) {
  return is_array($element) && !empty($element['enabled']) && $element['enabled'] == TRUE;
}

/**
 * Helper function for generating image token.
 *
 * Generates a token to protect a video style derivative. Same as the image
 * module's, but defined here in case the image module isn't enabled.
 *
 * This prevents unauthorized generation of an image style derivative,
 * which can be costly both in CPU time and disk space.
 *
 * @param string $style_name
 *   The name of the image style.
 * @param string $uri
 *   The URI of the image for this style, for example as returned by
 *   image_style_path().
 *
 * @return string
 *   An eight-character token which can be used to protect image style
 *   derivatives against denial-of-service attacks.
 *
 * @see image_style_path_token
 */
function _gifv_path_token($style_name, $uri) {
  if (module_exists('image')) {
    module_load_include('module', 'image');
    return image_style_path_token($style_name, $uri);
  }
  // Return the first eight characters.
  return substr(drupal_hmac_base64($style_name . ':' . $uri, drupal_get_private_key() . drupal_get_hash_salt()), 0, 8);
}

/**
 * Helper function to determine if a gif image is animated or not.
 *
 * From https://php.net/manual/en/function.imagecreatefromgif.php#104473
 *
 * @param mixed $file
 *   Either a string to the path to the file or a file array.
 *
 * @return bool
 *   TRUE if the gif image at $path is animated, FALSE otherwise.
 */
function _gifv_is_gif_animated(&$file) {
  $isarray = FALSE;
  if (is_array($file)) {
    $isarray = TRUE;
    if (isset($file['is_animated'])) {
      return $file['is_animated'];
    }
    if (!isset($file['location'])) {
      return FALSE;
    }
  }
  $path = $isarray ? $file['location'] == 'local' ? $file['uri'] : $file['source_image'] : $file;
  if (!($fh = @fopen($path, 'rb'))) {
    if ($isarray) {
      $file['is_animated'] = FALSE;
    }
    return FALSE;
  }
  $count = 0;
  while (!feof($fh) && $count < 2) {
    // Read 100kb at a time.
    $chunk = fread($fh, 1024 * 100);
    $count += preg_match_all('#\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)#s', $chunk, $matches);
  }
  fclose($fh);
  $status = $count > 1;
  if ($isarray) {
    $file['is_animated'] = $status;
  }
  return $status;
}
