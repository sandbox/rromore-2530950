<?php

/**
 * @file
 * Holds functions related to the module administration form.
 */

/**
 * Retrieve settings for the GifV toolkit.
 */
function gifv_settings($form, $form_state, $build_form = TRUE, $parents = array()) {
  module_load_include('inc', 'gifv', 'includes/gifv.utility');
  $default_wrapper = _gifv_get_default_wrapper_name();
  if (empty($default_wrapper)) {
    drupal_set_message(t('No wrappers were found. Please ensure you install at least one. This module comes with a submodule called "gifv_default_wrapper" which uses ffmpeg and provides a default wrapper implementation.'), 'warning', FALSE);
    return;
  }
  $defaults = _gifv_get_default_settings();
  $settings = !empty($form_state['settings']) ? array_merge($defaults, $form_state['settings']) : $defaults;

  if ($build_form) {
    $form['#validate'][] = '_gifv_settings_validate';

    $form['gifv_convert_on_upload'] = array(
      '#type' => 'checkbox',
      '#title' => t('Convert animated gif images to videos on image upload.'),
      '#default_value' => $settings['gifv_convert_on_upload'],
      '#description' => t('When animated gif images are uploaded via an image field, their video counterpart files will be automatically generated.'),
    );
    $form['gifv_convert_on_path'] = array(
      '#type' => 'checkbox',
      '#title' => t("Convert animated gif images to videos if they don't exist when a user visits that video file."),
      '#default_value' => $settings['gifv_convert_on_path'],
      '#description' => t("When an animated gif images counterpart video file is visited, but it has not yet been created, generate the video file. Won't work for derivative images unless the 'Allow insecure derivatives to be created' option in the 'Image Derivative Support' section below is enabled."),
    );
    $form['gifv_convert_on_path_all'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create all video types that are enabled in the settings when a user visits any video file.'),
      '#description' => t('If disabled, only video files that are visited will be created.'),
      '#default_value' => $settings['gifv_convert_on_path_all'],
      '#states' => array(
        'visible' => array(
          'input[name="gifv_convert_on_path"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['gifv_videos_filepath_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Store created videos in specified folder.'),
      '#description' => t('Instead of placing created videos in the same directory as its gif counterpart file, the video will be stored in a specified directory. <strong>Note:</strong> if image derivative support is enabled, the directory path will match the path of the derivative image.<br/><br/>For example, an animated gif image in public://styles/thumbnail/public/ will have its video counterpart files placed in public://videos/styles/thumbnail/public if the path to store videos is public://videos.'),
      '#default_value' => $settings['gifv_videos_filepath_enable'],
    );
    $form['gifv_videos_filepath'] = array(
      '#type' => 'textfield',
      '#title' => t('Location to store created videos'),
      '#description' => '',
      '#default_value' => $settings['gifv_videos_filepath'],
      '#states' => array(
        'visible' => array(
          'input[name="gifv_videos_filepath_enable"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['gifv_debug'] = array(
      '#type' => 'checkbox',
      '#title' => t('Debug'),
      '#description' => t('Enable to log debugging information in watchdog when videos are created.'),
      '#default_value' => $settings['gifv_debug'],
    );
  }
  $form['derivative_support'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image Derivative Support'),
    '#collapsible' => TRUE,
    '#collapsed' => !$build_form,
    '#description' => '',
  );
  $form['derivative_support']['gifv_support_image_derivatives'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include support for image style derivatives of animated gif images.'),
    '#default_value' => $settings['derivative_support']['gifv_support_image_derivatives'],
    '#description' => t('An image style derivative is any image generated with an image style via the Image module. !documentation', array(
      '!documentation' => l(t('Documentation provided here.'), 'https://www.drupal.org/documentation/modules/image', array(
        'attributes' => array(
          'target' => '_blank',
        ),
      )),
    )),
  );
  if ($build_form) {
    $form['derivative_support']['gifv_allow_insecure_derivatives'] = array(
      '#type' => 'checkbox',
      '#title' => t("Allow insecure derivatives to be created, and don't require a security key."),
      '#description' => t("Similar to the image module's security token, this parameter must be present in the path in order for the video file to be generated. Keeping this option disabled helps prevent DDoS attacks by requiring a security token (generated by Drupal at a different point in the code execution) to be present. For more information regarding security tokens, see the !release_notes.", array(
        '!release_notes' => l(t('Drupal 7.20 release notes'), 'https://www.drupal.org/drupal-7.20-release-notes', array(
          'attributes' => array('target' => '_blank'),
          'html' => TRUE,
        )),
      )),
      '#default_value' => $settings['derivative_support']['gifv_allow_insecure_derivatives'],
      '#states' => array(
        'visible' => array(
          'input[name="gifv_convert_on_path"]' => array('checked' => TRUE),
        ),
      ),
    );
  }
  $form['derivative_support']['gifv_support_image_derivatives_type'] = array(
    '#type' => 'radios',
    '#title' => t('Image Derivative Support Type'),
    '#default_value' => $settings['derivative_support']['gifv_support_image_derivatives_type'],
    '#description' => t("In most cases, it's enough to use the original derivative animated gif image as a source, converted to video. However, there are times when the derivative video might have visual artifacts created during the video processing. In these cases, selecting the latter of these options will cause the module to use the source animated gif image and apply the original derivative style's effects during the video files' creation. If the 'gifv_pernode' module is enabled, this is configurable on a per-node basis."),
    '#options' => array(
      'convert' => t('Convert the derivative image to video.'),
      'copy_image_effects' => t("Use the original source image's derivative style effects, translated to the wrapper's filters (not all wrappers guaranteed to be supported)."),
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="gifv_support_image_derivatives"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['wrappers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Wrapper'),
    '#collapsible' => TRUE,
    '#collapsed' => !$build_form,
    '#description' => t("Select the wrapper you'd like to use."),
  );

  $wrappers = _gifv_get_wrappers();
  $options = array();
  $wrapper_options = array();
  $video_options = array();
  $wrapper_option_children = array();

  foreach ($wrappers as $module => $module_wrapper) {
    foreach ($module_wrapper as $wrapper_id => $wrapper) {
      $id = $module . '__' . $wrapper_id;
      $options[$module][$id] = $wrapper['name'];
      if (empty($wrapper['options']) || !is_array($wrapper['options'])) {
        $wrapper['options'] = array();
      }
      $wrapper['options'] += array(
        '#type' => 'fieldset',
        '#title' => t('Wrapper Options for %name', array('%name' => $wrapper['name'])),
        '#description' => $wrapper['description'],
        '#collapsible' => TRUE,
        '#collapsed' => !$build_form,
        '#states' => array(
          'visible' => array(
            ':input[name="gifv_wrapper"]' => array('value' => $id),
          ),
        ),
      );
      $wrapper_option_children = element_children($wrapper['options']);
      if (count($wrapper_option_children)) {
        foreach ($wrapper_option_children as $wrapper_option_key) {
          $wrapper['options']['gifv_wrapper_options__' . $id . '__' . $wrapper_option_key] = $wrapper['options'][$wrapper_option_key];
          unset($wrapper['options'][$wrapper_option_key]);
        }
      }
      $wrapper_options[$module][$wrapper_id] = $wrapper['options'];
      if (!empty($settings['wrappers'][$id]['video_formats'])) {
        foreach ($settings['wrappers'][$id]['video_formats'] as $format => $formatter) {
          // Remove any formats from settings that aren't actually available.
          if (empty($wrapper['video_formats'][$format])) {
            unset($settings['wrappers'][$id]['video_formats'][$format]);
            continue;
          }
          drupal_array_merge_deep($wrapper['video_formats'][$format], $settings['wrappers'][$id]['video_formats'][$format]);
        }
      }
      $video_options[$wrapper_id] = array(
        'module' => $module,
        'rows' => $wrapper['video_formats'],
      );
    }
    if (!empty($wrapper['validate_hook'])) {
      $form['##validators'][$id] = array(
        'module' => $module,
        'file' => $wrapper['file'],
        'hook' => $wrapper['validate_hook'],
      );
    }
  }

  if ($build_form) {
    $form['wrappers']['gifv_wrapper'] = array(
      '#type' => 'select',
      '#multiple' => FALSE,
      '#title' => 'Wrapper',
      '#options' => $options,
      '#default_value' => $settings['wrappers']['gifv_wrapper'],
    );
  }

  $header = array(
    array('data' => NULL, 'colspan' => 2),
    array('class' => 'select-all'),
    t('Name'),
  );
  $tablerows = array();
  $row_elements = array();
  $video_format_settings = array();

  $form['wrappers']['video_formats'] = array(
    '#type' => 'fieldset',
    '#title' => t('Video Formats'),
    '#collapsible' => TRUE,
    '#collapsed' => !$build_form,
    '#description' => t("<p>Select which video formats you'd like to use, and the weights representing when they will be rendered. For example, most browsers can support MP4's, but not all browsers can support WebM. When WebM is before MP4, the browser will automatically attempt to load that file, and if it can't, it will move on to the next source. Generally, the more likely a browser will be able to support a format, the lower in the table it should be, and the higher the weight value it should have.</p>"),
  );

  $total_video_formatters = 0;
  foreach ($video_options as $vid => $video_option) {
    $module = $video_option['module'];
    $vid = $module . '__' . $vid;
    if (count($video_option['rows'])) {
      $total_video_formatters += count($video_option['rows']);
      foreach ($video_option['rows'] as $id => $entry) {
        $tablerows[$vid][$id] = array(
          'data' => array(
            // Cell for the cross drag&drop element.
            array(
              'class' => array('entry-cross'),
            ),
            // Weight item for the tabledrag.
            array(
              'data' => array(
                '#type' => 'weight',
                '#title' => t('Weight'),
                '#title_display' => 'invisible',
                '#default_value' => $entry['weight'],
                '#parents' => array_merge($parents, array(
                  'gifv_video_formats',
                  $vid,
                  'video_formats',
                  $id,
                  'weight',
                )),
                '#attributes' => array(
                  'class' => array('entry-order-weight'),
                ),
              ),
            ),
            // Enabled checkbox.
            array(
              'data' => array(
                '#type' => 'checkbox',
                '#title' => t('Enable'),
                '#title_display' => 'invisible',
                '#default_value' => $entry['enabled'],
                '#parents' => array_merge($parents, array(
                  'gifv_video_formats',
                  $vid,
                  'video_formats',
                  $id,
                  'enabled',
                )),
              ),
            ),
            // Name textfield.
            array(
              'data' => array(
                '#type' => 'item',
                '#markup' => $id,
                '#parents' => array_merge($parents, array(
                  'gifv_video_formats',
                  $vid,
                  'video_formats',
                  $id,
                  'name',
                )),
              ),
            ),
          ),
          'class' => array('draggable'),
        );
        // Build rows of the form elements in the table.
        $row_elements[$vid][$id] = array(
          'weight' => &$tablerows[$vid][$id]['data'][1]['data'],
          'enabled' => &$tablerows[$vid][$id]['data'][2]['data'],
          'name' => &$tablerows[$vid][$id]['data'][3]['data'],
        );

        if (empty($entry['options'])) {
          $entry['options'] = array();
        }
        // Update the #parents of the options.
        if (empty($entry['options']['#parents'])) {
          $entry['options']['#parents'] = array_merge($parents, array(
            $vid,
            'video_formats',
            $id,
            'options',
          ));
        }
        // Update the #parents and #default_value of each child option.
        // @todo move the parent into this function.
        _gifv_array_update_parents($entry['options'], $entry['options']['#parents'], TRUE);
        $video_format_settings[$vid][$id] = $entry['options'];
      }
      $form['wrappers']['video_formats']['gifv_video_formats'][$vid] = array(
        '#theme' => 'table',
        'elements' => $row_elements[$vid],
        '#rows' => $tablerows[$vid],
        '#header' => $header,
        '#attributes' => array(
          'id' => 'gifv-entry-order-' . $vid,
        ),
      );
      $form['wrappers']['video_formats']['gifv_video_formats_settings'] = $video_format_settings;
    }
    // @todo possibly do this only once and then when the wrapper changes,
    // execute the javascript again client-side.
    drupal_add_tabledrag('gifv-entry-order-' . $vid, 'order', 'sibling', 'entry-order-weight');
  }

  if ($total_video_formatters) {
    drupal_add_js('misc/tableselect.js');
  }
  else {
    drupal_set_message(t('No video formats could be found.'), 'warning', FALSE);
    $form['wrappers']['video_formats']['not_available'] = array(
      '#type' => 'markup',
      '#markup' => t('No video formats could be found.'),
    );
  }

  if ($build_form) {
    $form['wrappers']['gifv_wrapper_options'] = $wrapper_options;
    $form['remote'] = array(
      '#type' => 'fieldset',
      '#title' => t('Remote options'),
      '#collapsible' => TRUE,
      '#collapsed' => !$build_form,
      '#description' => '',
    );
    $form['remote']['gifv_remote_management'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create video files of remote animated gif images'),
      '#description' => t('Enables support of remote gif images, saving their video counterparts to the local server.'),
      '#default_value' => $settings['remote']['gifv_remote_management'],
    );
    $form['remote']['gifv_save_remote'] = array(
      '#type' => 'checkbox',
      '#title' => t('Save remote animated gif images to your server'),
      '#description' => t('When a remote animated gif image is used, save the file to your server and replace any instances to the remote location with the local location.'),
      '#states' => array(
        'visible' => array(
          'input[name=gifv_remote_management]' => array('checked' => TRUE),
        ),
      ),
      '#default_value' => $settings['remote']['gifv_save_remote'],
    );
    $form['remote']['gifv_save_remote_managed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Manage saved remote files'),
      '#description' => t('When enabled, remote files saved to your server will have an entry added to the "file_managed" table in your database.'),
      '#default_value' => $settings['remote']['gifv_save_remote_managed'],
      '#states' => array(
        'visible' => array(
          'input[name=gifv_remote_management]' => array('checked' => TRUE),
          'input[name="gifv_save_remote"]' => array('checked' => TRUE),
        ),
      ),
    );
  }
  $form['video'] = array(
    '#type' => 'fieldset',
    '#title' => t('Video tag options'),
    '#collapsible' => TRUE,
    '#collapsed' => !$build_form,
    '#description' => t('Documentation for the following options can be viewed at !video_docs.<br /><br />', array(
      '!video_docs' => l(t('https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video'), 'https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video', array(
        'attributes' => array(
          'target' => '_blank',
        ),
      )),
    )),
  );
  $form['video']['gifv_video_controls'] = array(
    '#type' => 'checkbox',
    '#title' => t('controls'),
    '#description' => t('Display video controls'),
    '#default_value' => $settings['video']['gifv_video_controls'],
  );
  $form['video']['gifv_video_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('autoplay'),
    '#description' => t('Autoplays the video'),
    '#default_value' => $settings['video']['gifv_video_autoplay'],
  );
  $form['video']['gifv_video_loop'] = array(
    '#type' => 'checkbox',
    '#title' => t('loop'),
    '#description' => t('Loops the video'),
    '#default_value' => $settings['video']['gifv_video_loop'],
  );

  $form['#attached'] = array(
    'css' => array(
      drupal_get_path('module', 'gifv') . '/css/gifv.admin.css',
    ),
    'js' => array(
      drupal_get_path('module', 'gifv') . '/js/gifv.admin.js',
    ),
  );

  $form['#attributes']['class'][] = 'gifv-configure';

  if ($build_form) {
    $form['#submit'][] = '_gifv_settings_submit';
    return system_settings_form($form);
  }
  return $form;
}

/**
 * Adjust form variables on submit.
 *
 * Since we're not using an actual field type for the video formats and weights,
 * we need to customize where it will be stored.
 */
function _gifv_settings_submit($form, &$form_state) {
  $keys = array_keys($form_state['values']);
  $keys = array_filter($keys, function ($entry) {
    return strpos($entry, 'gifv_wrapper_options__') !== FALSE;
  });
  foreach ($keys as $original_key) {
    $key = explode('__', $original_key);
    $pointer = &$form_state['values'][$key[0]];
    array_shift($key);
    if (count($key) > 0) {
      foreach ($key as $k) {
        if (!is_array($pointer) && empty($pointer)) {
          $pointer = array();
        }
        if (empty($pointer[$k])) {
          $pointer[$k] = array();
        }
        $pointer = &$pointer[$k];
      }
    }
    $pointer = $form_state['values'][$original_key];
    unset($form_state['values'][$original_key]);
  }

  module_load_include('inc', 'gifv', 'includes/gifv.utility');
  $keys = array_keys($form_state['values']['gifv_video_formats']);
  foreach ($keys as $key) {
    if (!empty($form_state['values'][$key])) {
      drupal_array_merge_deep($form_state['values']['gifv_video_formats'][$key], $form_state['values'][$key]);
      unset($form_state['values'][$key]);
    }
  }
  cache_clear_all('gifv', 'cache', TRUE);
  cache_clear_all('gifv_wrappers_info', 'cache');
}

/**
 * Validate form settings.
 *
 * Loads the enabled wrapper's implementation and calls its static validateForm
 * method with the $form and $form_state parameters.
 */
function _gifv_settings_validate($form, &$form_state) {
  $wrapper_id = $form_state['values']['gifv_wrapper'];
  // Load the wrapper's validator function and call it on the parameters passed
  // to this function.
  if (!empty($form['##validators'][$wrapper_id])) {
    $options = array_keys($form_state['values']);
    $options = array_filter($options, function ($entry) {
      return strpos($entry, 'gifv_wrapper_options') !== FALSE;
    });
    $wrapper_options = array();
    foreach ($options as $option) {
      $parts = explode('__', $option);
      $wrapper_options[$parts[count($parts) - 1]] = array('value' => $form_state['values'][$option], 'form_element' => $option);
    }

    $validator = $form['##validators'][$wrapper_id];
    module_load_include('inc', $validator['module'], $validator['file']);
    $parts = explode('::', $validator['hook']);
    $parts[0]::$parts[1]($form, $form_state, $wrapper_options);
  }
  // If the path to store videos is set, make sure the paths exist and are
  // writable. If not, try to create it, or make it writable. If unable to, set
  // an error on the form.
  if ($form_state['values']['gifv_videos_filepath_enable'] == TRUE) {
    $directory = $form_state['values']['gifv_videos_filepath'];
    $absolute_path = $directory[0] == '/' ? $directory : $_SERVER['DOCUMENT_ROOT'] . '/' . variable_get('file_public_path', conf_path() . '/files') . '/' . str_replace('public://', '', $directory);
    if (!file_prepare_directory($absolute_path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      $message = t("The directory you specified to store videos doesn't exist or is not writable.");
      form_set_error('gifv_videos_filepath', $message);
    }
  }
}

/**
 * Helper function to update a form array's parents to their correct values.
 *
 * Because we let the wrapper define form elements in terms of their own local
 * scopes, instead of needing to know the module name and wrapper id we assign
 * to each wrapper, the form we receive back from the wrapper will have
 * incorrect #parents and #states values. This function will walk along the
 * form's elements, and update the #parents and any #states selector values of
 * each element to their correct values.
 *
 * @param array &$element
 *   The form element to traverse.
 * @param array &$parents
 *   A predefined list of parents to prepend to each element.
 * @param bool $root
 *   Flag indicating that $element is the original element passed to this
 *   function.
 * @param array &$original_element
 *   The original element.
 *
 * @return array
 *   The element variable, with each element having a modified value of their
 *   #parents and #states values.
 */
function _gifv_array_update_parents(array &$element, array &$parents, $root, array &$original_element = NULL) {
  if (!is_array($element)) {
    throw new Error('Invalid parameter provided, expected an array.');
  }
  if ($original_element == NULL && $root) {
    $original_element = &$element;
  }
  $children = element_children($element);
  if (($uidpos = array_search('uid', $children)) !== FALSE) {
    unset($children[$uidpos]);
  }

  if (count($children) == 0) {
    return $element;
  }

  foreach ($children as $key) {
    array_push($parents, $key);
    if (is_array($element[$key]) && empty($element[$key]['#parents'])) {
      $element[$key]['#parents'] = $parents;
    }
    $parents_string = $parents[0] . '[' . implode('][', array_slice($parents, 1, count($parents) - 1)) . ']';
    if (is_array($element[$key]) && empty($element[$key]['##uid'])) {
      $element[$key]['##uid'] = $parents_string;
    }

    if (!empty($element[$key]['#states'])) {
      foreach ($element[$key]['#states'] as $statekey => $statevalue) {
        foreach ($statevalue as $selectorkey => $selectorvalue) {
          if (is_int($selectorkey)) {
            // If there are multiple conditions, need to visit each condition.
            foreach (array_keys($selectorvalue) as $newselectorkey) {
              _gifv_update_states($element, $original_element, $key, $statekey, $newselectorkey, $selectorkey);
            }
          }
          else {
            _gifv_update_states($element, $original_element, $key, $statekey, $selectorkey);
          }
        }
      }
    }
    _gifv_array_update_parents($element[$key], $parents, FALSE, $original_element);
    array_pop($parents);
  }
}

/**
 * Helper function to update a form parent's state.
 *
 * This function is called from _gifv_array_update_parents, and will set the
 * #states value to something more appropriate matching the element's location
 * in the array.
 *
 * @see _gifv_array_update_parents()
 */
function _gifv_update_states(&$element, $original_element, $key, $statekey, $selectorkey, $conditionalkey = NULL) {
  $match = preg_match_all('/\=\"?([\w]+)(\[([\w\[\]]+)\])*\"?/i', $selectorkey, $matches);
  if ($match !== FALSE && $match !== 0) {
    $parent = $matches[1][0];
    $els = explode('][', $matches[3][0]);
    if (!empty($original_element[$parent])) {
      $pointer = $original_element[$parent];
      $pointer_string = $parent;
      if (count($els)) {
        foreach ($els as $el) {
          if (!empty($pointer[$el])) {
            $pointer = $pointer[$el];
            $pointer_string .= '[' . $el . ']';
          }
        }
      }
      $newkey = $matches[1][0];
      if (drupal_strlen($matches[3][0]) > 0) {
        $newkey .= '[' . $matches[3][0] . ']';
      }
      $newkey = str_replace($newkey, $pointer['##uid'], $selectorkey);
      $target = &$element[$key]['#states'][$statekey];
      if (isset($conditionalkey)) {
        $target = &$target[$conditionalkey];
      }
      $target[$newkey] = $target[$selectorkey];
      unset($target[$selectorkey]);
    }
  }
}
