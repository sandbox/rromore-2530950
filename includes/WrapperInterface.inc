<?php

/**
 * @file
 * Provides an interface of required methods a wrapper needs.
 */

interface WrapperInterface {
  /**
   * Gets the name of the wrapper.
   * @return string
   *   Name of the wrapper.
   */
  public function getName();

  /**
   * Gets the description of the wrapper.
   * @return string
   *   Description of the wrapper.
   */
  public function getDescription();

  /**
   * Gets the available video formats.
   * @return array
   *   An array of the available video formats.
   */
  public function getAvailableVideoFormats();

  /**
   * Static function the module will call to validate configuration settings.
   *
   * @param array $form
   *   The form element.
   * @param array &$form_state
   *   The form state elements.
   * @param array $wrapper_options
   *   The wrapper options for this form.
   */
  public static function validateForm($form, &$form_state, $wrapper_options = array());

  /**
   * Gets the wrapper options.
   *
   * @return array
   *   An array of wrapper options.
   */
  public function getOptions();

  /**
   * Creates videos at $destinations from $file.
   *
   * @param array $file
   *   An array of file information.
   * @param array $destinations
   *   An array of file destinations keyed by type.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function save($file, $destinations);
}
