<?php

/**
 * @file
 * Holds functions for theming gifv output.
 */

/**
 * Returns HTML for an image url field formatter.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An array of image data.
 *   - image_style: An optional image style.
 *   - path: An array containing the link 'path' and link 'options'.
 *
 * @ingroup themeable
 */
function theme_gifv_image_formatter(array $variables) {
  $item = $variables['item'];
  $style = '';
  if (!empty($variables['image_style'])) {
    $style = $variables['image_style'];
    $item['source_paths'] = array(
      'uri' => $item['uri'],
    );
    preg_match('/((private|public|temporary)\:\/\/)(.*)/i', $item['uri'], $matches);
    if (count($matches) == 4 && $wrapper = file_stream_wrapper_get_instance_by_uri($item['uri'])) {
      $item['source_paths']['absolute_path'] = $wrapper->realpath();
    }

    $item['uri'] = image_style_path($style, $item['uri']);
    if ($wrapper = file_stream_wrapper_get_instance_by_uri($item['uri'])) {
      $item['absolute_path'] = $wrapper->realpath();
    }

    $item['is_derivative'] = TRUE;
    $item['image_style'] = $variables['image_style'];
    image_style_transform_dimensions($item['image_style'], $item);
  }

  module_load_include('inc', 'gifv', 'includes/gifv.utility');
  // Make sure the $item variable has all of the correct information.
  _gifv_get_image_info($item);

  $image = array(
    'path' => $item['uri'],
  );

  if (!empty($item['alt'])) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }

  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title'])) {
    $image['title'] = $item['title'];
  }

  if ($variables['image_style']) {
    $image['style_name'] = $variables['image_style'];
    $output = theme('image_style', $image);
  }
  else {
    $output = theme('image', $image);
  }

  // The link path and link options are both optional, but for the options to be
  // processed, the link path must at least be an empty string.
  if (!empty($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $options = !empty($variables['path']['options']) ? $variables['path']['options'] : array();
    // When displaying an image inside a link, the html option must be TRUE.
    $options['html'] = TRUE;
    if ($item['mime_type'] !== 'image/gif') {
      // Wait until we render the video element.
      $output = l($output, $path, $options);
    }
  }

  if (empty($item['gifv']) || !empty($item['gifv']['enabled']) && !$item['gifv']['enabled']) {
    return $output;
  }

  $default_settings = _gifv_get_default_settings();
  $settings = !empty($item['gifv']['settings']) ? drupal_array_merge_deep($default_settings, $item['gifv']['settings']) : $default_settings;
  if ($item['mime_type'] == 'image/gif') {
    _gifv_generate_derivative_paths($item, $settings['wrappers'][_gifv_get_default_wrapper_name()]['video_formats']);
    if (!empty($style)) {
      // If the derivative image doesn't exist, use the image module to create
      // it before we attempt to use it.
      image_style_create_derivative(image_style_load($style), $item['source_paths']['uri'], $item['uri']);
    }
    $createvideos = array();
    foreach ($item['video_paths'] as $extension => $path) {
      if (!file_exists($path['uri'])) {
        $createvideos[] = $extension;
      }
      $videos['video/' . $extension] = file_create_url($path['uri']);
      if (!empty($style) && !$settings['derivative_support']['gifv_allow_insecure_derivatives']) {
        $videos['video/' . $extension] .= '?gvtok=' . _gifv_path_token($style, $path['uri']);
      }
    }
    if (count($createvideos)) {
      $image = _gifv_create_video($item, $settings, $createvideos);
    }
    return theme('gifv__video', array(
      'image' => $image,
      'video_attributes' => array(
        $settings['video']['gifv_video_controls'] == 1 ? 'controls' : '',
        $settings['video']['gifv_video_autoplay'] == 1 ? 'autoplay' : '',
        $settings['video']['gifv_video_loop'] == 1 ? 'loop' : '',
      ),
      'sources' => $videos,
      'output' => $output,
    ));
  }

  return $output;
}
