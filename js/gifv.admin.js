(function($) {
  $(document).ready(function() {
    var $select = $('select[name="gifv_wrapper"]');
    var gifv_change_video_formats_table = function($id) {
      $('table#gifv-entry-order-' + $id)
        .show()
        .siblings('table, div.tabledrag-toggle-weight-wrapper')
          .hide()
        .end()
        .prev('table')
          .prev('div.tabledrag-toggle-weight-wrapper')
          .show()
        ;
    };
    gifv_change_video_formats_table($select.val());
    $select.change(function() {
      gifv_change_video_formats_table($(this).val());
    });

    // Add an 'Options' column to the formatters table and an 'Edit' link in each row for this column.
    // Hide formatter settings, and when this edit link is clicked, show them.
    var $tables = $('#edit-video-formats, #edit-instance-widget-settings-gifv-advanced-settings-wrappers-video-formats, #edit-gifv-gifv-advanced-settings-wrappers-video-formats').find('table.tabledrag-processed.table-select-processed');
    $tables.find('thead > tr').append('<th>Options</th>');
    $tables.find('tbody > tr').each(function(index, value) {
      var $id = $(value).find('select.form-select').attr('id').replace(/edit\-gifv\-video\-formats/i, 'edit').replace(/\-weight$/i, '-options');
      if (index !== 0) {
        $('fieldset#'+$id).hide();
      }
      var $link = $('<a href="#">Edit</a>').click(function() {
        $('fieldset#'+$id).show().siblings('fieldset').hide();
        return false;
      });
      var $column = $('<td class="options"></td>').append($link);
      $(value).append($column);
    });
  });
})(jQuery);
