<?php

/**
 * @file
 * Documents API functions for the gifv module.
 */

/**
 * Defines a wrapper for gifv to use.
 *
 * @return array
 *   An array of options defining a wrapper for gifv.
 *   Array should be formatted as:
 *     [module_name] =>
 *       [wrapper_name] =>
 *         (required) [name],
 *         (optional) [options] => form element array defining options for your
 *                                 wrapper.
 *         (optional) [file] => a file defining your wrapper
 */
function callback_gifv_wrapper() {
  return array(
    'gifv' => array(
      'ffmpeg' => array(
        'name' => 'ffmpeg',
        'options' => array(
          array(
            '#type' => 'textfield',
            '#title' => 'test',
          ),
        ),
      ),
    ),
  );
}
