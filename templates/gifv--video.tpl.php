<?php
/**
 * @file
 * Displays a HTML5 video element with sources.
 *
 * Available variables:
 *   - $image: The image variable containing attributes about the image like
 *     uri, width, height, etc.
 *   - $sources: The available counterpart video sources of the image.
 *     Structured as an key-value array where the keys are the mime type and the
 *     values are the path.
 *   - $output: The rendered output, usually the image or the link and image.
 */
?>

<video <?php if (!empty($image['width'])): ?> width="<?php print $image['width']; ?>" <?php endif; ?> <?php if (!empty($image['height'])): ?> height="<?php print $image['height']; ?>" muted <?php endif; ?> <?php print implode(' ', $video_attributes); ?>>
<?php foreach ($sources as $mime => $source): ?>
  <source src="<?php print $source; ?>" type="<?php print $mime; ?>" />
<?php endforeach; ?>
<?php print $output; ?>
</video>
