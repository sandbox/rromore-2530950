The module configuration page for GifV can be accessed at admin/config/media/gifv.

This module comes with a submodule called "gifv_default_wrapper" which provides a default wrapper implementation for ffmpeg. At least one wrapper needs to be defined in order to use the main module.

The gifv_input_filter module will allow individual text formatters to convert inline gif images to videos, with configuration separate from the main module's configuration.

The gifv_pernode module will allow image fields (and optionally text fields if the gifv_input_filter module is enabled and configured) on individual nodes to be configured, with configuration separate from the main module's configuration.

The gifv_field_formatter_settings will extend the gifv options with an image field's options, allowing for this module's behavior to be configured on a per-field basis.